## NFS server firewall-friendly setup

Configures static ports and iptables rules for `statd/mountd/lockd`:

```yaml
statd_listen_port: 32765
statd_outgoing_port: 32766
mountd_port: 32767
lockd_port: 32768
```

Usage:

```yaml
- import_role:
    name: nfs_server
  vars:
    exports:
      - path: /srv/templates
        clients: "{{ subnets }}"
        options: "(rw,anonuid=999,anongid=999)"
```