#!/usr/bin/env python3


def do_map(*args, **kwargs):
    seq = args[0]
    if seq:
        for item in seq:
            yield item.get(kwargs['attribute'], kwargs['default'])


class FilterModule(object):
    def filters(self):
        return {'default_map': do_map}